function doIt() {
    var number1 = document.getElementById("number1").value;
    var number2 = document.getElementById("number2").value;
    var number3 = document.getElementById("number3").value;
    
    number1 = Number(number1);
    number2 = Number(number2);
    number3 = Number(number3);
    var sum = number1 + number2 + number3; 
    document.getElementById("answer").innerHTML = sum;
    
    if(sum >= 0) { 
        document.getElementById("answer").className = "positive";
    } else {
        document.getElementById("answer").className = "negative";
    }

    if(sum % 2 == 0) {
        document.getElementById("isEven").className = "even";
        document.getElementById("isEven").innerHTML = "(even)";
    } else {
        document.getElementById("isEven").className = "odd";
        document.getElementById("isEven").innerHTML = "(odd)";
    }

}