//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let arr = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
var arrP=[]
var arrN=[]
var arrPO=[]
var arrNE=[]

for (i=0;i<arr.length;i++)
{
    if (arr[i]>0)
    {
        arrP.push(arr[i])
    }
    else if (arr[i]<0)
    {
        arrN.push(arr[i])
    }
}


for (i=0;i<arrP.length;i++)
{
    if (arrP[i]%2 !== 0)
    {
        arrPO.push(arrP[i])
    }
}
for (i=0;i<arrN.length;i++)
{
    if (arrN[i]%2 === 0)
    {
        arrNE.push(arrN[i])
    }
}

output+="positive:"+"\n"
output+=arrPO+"\n"
output+="negative:"+"\n"
output+=arrNE
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    let D1=0
let D2=0
let D3=0
let D4=0
let D5=0
let D6=0

for (i=0;i<(60000);i++)
{
    R = Math.floor((Math.random() * 6) + 1)
    if (R==1)
    {
        D1++
    }
    else if (R==2)
    {
        D2++
    }
    else if (R==3)
    {
        D3++
    }
    else if (R==4)
    {
        D4++
    }
    else if (R==5)
    {
        D5++
    }
    else if (R==6)
    {
        D6++
    }
}

output+=('Frequency of die rolls'+"\n")
output+=('1: '+(D1)+"\n")
output+=('2: '+(D2)+"\n")
output+=('3: '+(D3)+"\n")
output+=('4: '+(D4)+"\n")
output+=('5: '+(D5)+"\n")
output+=('6: '+(D6)+"\n")
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    DR=[0,0,0,0,0,0,0]
console.log(DR)
for (i=0;i<(60000);i++)
{    
    R = Math.floor((Math.random() * 6) + 1)
    DR[R]++
}
    
output+="Frequency of die rolls"+"\n"    
for (i=1;i<(7);i++)
    {
    output+=(String(i) + ': ' + DR[i]+"\n")
    }
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var dieRolls = {
    Frequencies:{
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
    },
    Total:60000,
    Exceptions:"",
}

for (i=0;i<(dieRolls.Total);i++)
{    
    R = Math.floor((Math.random() * 6) + 1)
    dieRolls.Frequencies[R]++
    
}

for (i=1;i<7;i++)
{
    ABS=Math.abs(dieRolls.Frequencies[i]-10000)
    percent=ABS/10000
    if (percent > 0.01)
    {
        dieRolls.Exceptions=dieRolls.Exceptions+i+" "
    }
}

for (var obj in dieRolls){
    output+=(obj + (dieRolls[obj])+"\n")
}
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    var person = {
    name: "Jane",
    income: 127050
}

let tax = 0

for (i=0;i<(5);i++)
{
    if (person.income >= 180001){
        tax = 54097 + (person.income-180000)*0.45
    }
    else if (90001 <= person.income && person.income <= 180000){
        tax = 20797 + (person.income-90000)*0.37
    }    
    else if (37001 <= person.income && person.income <= 90000){
        tax = 3572 + (person.income-37000)*0.325
    }
    else if (18201 <= person.income && person.income <= 37000){
        tax = (person.income-18200)*0.19
    }    
    else{
        tax = 0
    }
}
output+=(person.name + "income is: $" + person.income + ", and her tax owed is :$" + tax)
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}