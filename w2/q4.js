var dieRolls = {
    Frequencies:{
        1:0,
        2:0,
        3:0,
        4:0,
        5:0,
        6:0,
    },
    Total:60000,
    Exceptions:"",
}

for (i=0;i<(dieRolls.Total);i++)
{    
    R = Math.floor((Math.random() * 6) + 1)
    dieRolls.Frequencies[R]++
    
}

for (i=1;i<7;i++)
{
    ABS=Math.abs(dieRolls.Frequencies[i]-10000)
    percent=ABS/10000
    if (percent > 0.01)
    {
        dieRolls.Exceptions=dieRolls.Exceptions+i+" "
    }
}

for (var obj in dieRolls){
    console.log(obj + (dieRolls[obj]))
}