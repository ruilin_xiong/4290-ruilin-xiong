var outputAreaRef = document.getElementById("outputArea3");
var output = "";

function flexible(fOperation, operand1, operand2)
{
    var result = fOperation(operand1, operand2);
    return result;
}

function sum(a,b)
{
    var sum = a + b
    return sum
}

function times(a,b)
{
    var times = a*b
    return times
}

output += flexible(sum,3,5) + "\n";
output += flexible(times,3,5) + "\n"; 
outputAreaRef.innerHTML = output;